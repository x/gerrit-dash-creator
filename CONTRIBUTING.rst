============
Contributing
============

If you would like to contribute to the development of OpenStack or
OpenStack-related projects like this one you must follow the steps
in this page:

   http://docs.openstack.org/infra/manual/developers.html

Once those steps have been completed, changes to OpenStack
should be submitted for review via the Gerrit tool, following
the workflow documented at:

   http://docs.openstack.org/infra/manual/developers.html#development-workflow

Pull requests submitted through GitHub will be ignored.

Bugs
====

This is an informal OpenStack-related project that doesn't have an official bug
tracker.  If you notice a problem, feel free to fix it and post a patch to
Gerrit (see above).  If you notice a problem but aren't sure how to fix it,
please mention it in #openstack-infra on Freenode, where you may get some
advice for fixing it, or you can at least let us know that there's an issue.
